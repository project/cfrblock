<?php

namespace Drupal\cfrblock\Block;

interface BlockInterface {

  /**
   * @return array
   *   Format: ['subject' => '..', 'content' => [..]]
   *
   * @see hook_block_view()
   */
  public function view();

}
