<?php

namespace Drupal\cfrblock\Block;

use Drupal\renderkit\BuildProvider\BuildProviderInterface;

/**
 * @CfrPlugin(
 *   id = "buildProvider",
 *   label = "Build provider",
 *   inline = true
 * )
 */
class Block_BuildProvider implements BlockInterface {

  /**
   * @var \Drupal\renderkit\BuildProvider\BuildProviderInterface
   */
  private $buildProvider;

  /**
   * @param \Drupal\renderkit\BuildProvider\BuildProviderInterface $buildProvider
   */
  public function __construct(BuildProviderInterface $buildProvider) {
    $this->buildProvider = $buildProvider;
  }

  /**
   * @return array
   *   Format: ['subject' => '..', 'content' => [..]]
   *
   * @see hook_block_view()
   */
  public function view() {
    return [
      'content' => [
        // This needs to be nested, to prevent _block_get_renderable_array()
        // from manipulating $element['#theme_wrappers'].
        'block_content' => $this->buildProvider->build(),
      ],
    ];
  }
}
